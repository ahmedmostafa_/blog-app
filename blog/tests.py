from django.test import TestCase, Client
from django.contrib.auth import get_user_model
from django.urls import reverse

from .models import Post
# Create your tests here.
class BlogTest(TestCase):

    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username = 'testuser',
            email = 'test@email.com',
            password = 'secret'
        )

        self.post = Post.objects.create(
            title = 'a nice title',
            text = 'a sexy text',
            author = self.user,
        )


    def test_string_representation(self):
        post = Post(title = 'A simple title')
        self.assertEqual(str(post), post.title)

    def test_post_content(self):
         self.assertEqual(f'{self.post.title}','a nice title')
         self.assertEqual(f'{self.post.author}', 'testuser')
         self.assertEqual(f'{self.post.text}', 'a sexy text')

    def test_post_list_view(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'a sexy text')
        self.assertTemplateUsed(response, 'home.html')

    def test_post_detail_view(self):
        response = self.client.get('/post/1/')
        no_response = self.client.get('/post/100000/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, 'a sexy text')
        self.assertTemplateUsed(response, 'post_detail.html')

    def test_post_create_view(self):
        response = self.client.post(reverse('post_new'),{
            'title': 'New title',
            'text': 'New text',
            'author': self.user,
        })

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'New title')
        self.assertContains(response, 'New text')


    def test_post_update_view(self):
        response = self.client.post(reverse('post_edit', args='1'),{
            'title':'updated title',
            'text':'updated text'
        })

        self.assertEqual(response.status_code, 302)

    def test_post_delete_view(self):
         response = self.client.get(reverse('post_delete', args='1'))
         self.assertEqual(response.status_code, 200)
